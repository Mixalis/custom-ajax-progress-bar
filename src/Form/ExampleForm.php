<?php

namespace Drupal\custom_ajax_progress_bar\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExampleForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'example_ajax_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['textfield'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field progress bar default'),
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'event' => 'keydown',
      ],
    ];

    $form['textfield2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field progress bar custom message'),
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'event' => 'keydown',
        'progress' => [
          'type' => 'throbber',
          'message' => 'Custom message',
        ],
      ],
    ];

    $form['textfield3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field progress bar fullscreen'),
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'event' => 'keydown',
        'progress' => [
          'type' => 'fullscreen',
        ],
      ],
    ];

    $form['textfield4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field with percentage progress bar'),
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'event' => 'keydown',
        'progress' => [
          'type' => 'bar',
          'url' => Url::fromRoute('custom_ajax_progress_bar.ajax_progress', ['arg1' => rand(1,10)]),
          'interval' => 1000
        ],
      ],
    ];

    $form['field_custom_progress_bar'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field with custom progress bar'),
      '#ajax' => [
        'callback' => '::myAjaxCallback',
        'event' => 'keydown',
        'progress' => [
          'type' => 'myCustomProgressBar',
          'message' => 'Waiting and envoy my progress bar !',
          'option_custom' => 'Oh yeah !'
        ],
      ],
    ];

    $form['#attached']['library'][] = 'custom_ajax_progress_bar/ajax_progress_bar.myAwesomeProgressBar';

    return $form;
  }

  public function myAjaxCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    sleep(5);
    return $response;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
