<?php

namespace Drupal\custom_ajax_progress_bar\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines a controller to respond to file widget AJAX requests.
 */
class CustomAjaxProgressBarController {

  /**
   * Returns the progress status for a process bar.
   *
   * @param string $arg1
   *   Random int;
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JsonResponse object.
   */
  public function progress($arg1) {
    $progress = [
      'message' => 'Argument passé à la méthode = ' . $arg1,
      'percentage' => rand(0,100),
    ];

    return new JsonResponse($progress);
  }

}
