(function ($, Drupal) {

  Drupal.theme.ajaxProgressBarMycustomprogressbar = function (message, option_custom) {
    var throbber = '<span class="throbber"><img src="https://loading.io/spinners/color-bar/index.colorful-progress-loader.gif" /></span>';

    return '<span class="ajax-progress ajax-progress-custom">' + throbber + message + '  --- <span>Custom option : ' + option_custom + '</span>' + throbber +'</div>';
  };

  Drupal.Ajax.prototype.setProgressIndicatorMycustomprogressbar = function () {
    this.progress.element = $(Drupal.theme('ajaxProgressBarMycustomprogressbar', this.progress.message, this.progress.option_custom));
    $(this.element).after(this.progress.element);
  };

})(jQuery, Drupal);
